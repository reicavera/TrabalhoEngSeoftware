import { Injectable, UnauthorizedException } from '@nestjs/common';
import { LoginDTO } from './dto/login.dto';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { UsuarioService } from 'src/usuario/usuario.service';
import { ObterPeloCpfDTO } from 'src/usuario/dto/obterPeloCpf.dto';
import { autenticarEmail } from 'src/email/email.service';
import { RedefinirSenhaDTO } from './dto/redefinirSenha.dto';
import { randomBytes } from 'crypto';

@Injectable()
export class AcessoService {
  constructor(
    private usuarioService: UsuarioService,
    private jwtService: JwtService,
  ) {}
  async validacao(validacaoDTO: LoginDTO) {
    const obterPeloCPF: ObterPeloCpfDTO = {
      cpf: validacaoDTO.cpf,
    };
    const usuario = await this.usuarioService.obterPeloCPF(obterPeloCPF);
    if (usuario && bcrypt.compareSync(validacaoDTO.senha, usuario.senha)) {
      const refresh = randomBytes(64).toString('hex');
      const payload = {
        sub: usuario.cpf,
        cargo: usuario.cargo,
        tipo: 'usuario',
        instituicao: usuario.instituicao,
        refresh,
      };
      await this.usuarioService.alterarRefresh(usuario.id, refresh);
      return payload;
    }
    return undefined;
  }
  async autenticarEmail(obterPeloCPF: ObterPeloCpfDTO) {
    const usuario = await this.usuarioService.obterPeloCPF(obterPeloCPF);
    if (usuario) {
      const refresh = randomBytes(64).toString('hex');
      const payload = {
        sub: usuario.cpf,
        cargo: usuario.cargo,
        tipo: 'email',
        refresh,
      };
      await this.usuarioService.alterarRefresh(usuario.id, refresh);
      autenticarEmail(
        usuario.email,
        this.jwtService.sign(payload, { expiresIn: 60 * 15 }),
      );
    }
  }
  async login(validacaoDTO: LoginDTO) {
    const payload = await this.validacao(validacaoDTO);
    if (!payload) {
      throw new UnauthorizedException('CPF ou senha inválidos');
    }
    return {
      acessoToken: this.jwtService.sign(payload),
    };
  }
  async refresh(cpf: string) {
    const usuario = await this.usuarioService.obterPeloCPF({ cpf });
    const refresh = randomBytes(64).toString('hex');
    const payload = {
      sub: usuario.cpf,
      cargo: usuario.cargo,
      tipo: 'usuario',
      instituicao: usuario.instituicao,
      refresh,
    };
    await this.usuarioService.alterarRefresh(usuario.id, refresh);
    return {
      acessoToken: this.jwtService.sign(payload),
    };
  }
  async logout(cpf: string) {
    const usuario = await this.usuarioService.obterPeloCPF({ cpf });
    this.usuarioService.alterarRefresh(usuario.id, null);
  }
  async novaSenha(cpf: string) {
    const usuario = await this.usuarioService.obterPeloCPF({ cpf });
    if (usuario) {
      this.usuarioService.novaSenha(usuario.cpf);
    }
    return;
  }
  async redefinirSenha(cpf: string, redefinirSenhaDTO: RedefinirSenhaDTO) {
    this.usuarioService.redefinirSenha(cpf, redefinirSenhaDTO);
  }
}
