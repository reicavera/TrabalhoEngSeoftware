import { IsNotEmpty, IsString } from 'class-validator';

export class RedefinirSenhaDTO {
  @IsNotEmpty()
  @IsString()
  readonly senha: string;
}
