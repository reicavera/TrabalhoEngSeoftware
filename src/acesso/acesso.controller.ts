import { Body, Controller, Patch, Post, UseGuards } from '@nestjs/common';
import { ObterPeloCpfDTO } from '../usuario/dto/obterPeloCpf.dto';
import { AcessoService } from './acesso.service';
import { UsuarioCpf } from './decorator/usuario.decorator';
import { RedefinirSenhaDTO } from './dto/redefinirSenha.dto';
import { LoginDTO } from './dto/login.dto';
import { JwtAuthGuard } from './jwt/jwt-auth.guard';
import { JwtEmailGuard } from './jwt/jwt-email.guard';

@Controller('acesso')
export class AcessoController {
  constructor(private acessoService: AcessoService) {}
  @Post('/login')
  login(@Body() validacaoDTO: LoginDTO) {
    return this.acessoService.login(validacaoDTO);
  }

  @Post('/refresh')
  @UseGuards(JwtAuthGuard)
  refresh(@UsuarioCpf() cpf: string) {
    return this.acessoService.refresh(cpf);
  }

  @Post('/logout')
  @UseGuards(JwtAuthGuard)
  logout(@UsuarioCpf() cpf: string) {
    return this.acessoService.logout(cpf);
  }

  @Post('/autenticarEmail')
  autenticarEmail(@Body() obterPeloCpfDTO: ObterPeloCpfDTO) {
    return this.acessoService.autenticarEmail(obterPeloCpfDTO);
  }
  @Patch('/novaSenha')
  @UseGuards(JwtEmailGuard)
  novaSenha(@UsuarioCpf() cpf: string) {
    return this.acessoService.novaSenha(cpf);
  }
  @Patch('/redefinirSenha')
  @UseGuards(JwtAuthGuard)
  redefinirSenha(@UsuarioCpf() cpf, redefinirSenhaDTO: RedefinirSenhaDTO) {
    this.acessoService.redefinirSenha(cpf, redefinirSenhaDTO);
  }
}
