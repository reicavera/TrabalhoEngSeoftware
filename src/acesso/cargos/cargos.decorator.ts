import { SetMetadata } from '@nestjs/common';
import { Cargo } from '../../usuario/enum/cargos.enum';

export const ROLES_KEY = 'cargos';
export const Cargos = (...cargos: Cargo[]) => SetMetadata(ROLES_KEY, cargos);
