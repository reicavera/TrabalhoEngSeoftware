import { createParamDecorator, ExecutionContext } from '@nestjs/common';

const UsuarioCpf = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    return request.user.cpf;
  },
);
const UsuarioCargo = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    return request.user.cargo;
  },
);
export { UsuarioCargo, UsuarioCpf };
