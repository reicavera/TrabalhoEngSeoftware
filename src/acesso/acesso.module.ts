import { CacheModule, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AcessoController } from './acesso.controller';
import { AcessoService } from './acesso.service';
import 'dotenv/config';
import { UsuarioModule } from 'src/usuario/usuario.module';
import { JwtStrategy } from './jwt/jwt.strategy';
import { CargoGuard } from './cargos/cargos.guard';

@Module({
  imports: [
    UsuarioModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: 60 * 60 },
    }),
    CacheModule.register(),
  ],
  controllers: [AcessoController],
  providers: [AcessoService, JwtStrategy, CargoGuard],
  exports: [AcessoService],
})
export class AcessoModule {}
