import {
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class JwtEmailGuard extends AuthGuard('jwt') {
  canActivate(context: ExecutionContext) {
    return super.canActivate(context);
  }

  handleRequest(err, user, info) {
    if (err || !user) {
      throw (
        err || new UnauthorizedException('Bearer Token inexistente ou inválido')
      );
    }
    if (user.tipo != 'email') {
      throw new UnauthorizedException('Bearer Token do tipo errado');
    }
    return user;
  }
}
