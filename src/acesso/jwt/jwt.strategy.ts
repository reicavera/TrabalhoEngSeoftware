import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsuarioService } from 'src/usuario/usuario.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private usuarioService: UsuarioService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET,
    });
  }

  async validate(payload: any) {
    const usuario = await this.usuarioService.obterPeloCPF({
      cpf: payload.sub,
    });
    if (!usuario || payload.refresh !== usuario.refresh) {
      throw new UnauthorizedException('Bearer Token inválido');
    }
    return {
      cpf: payload.sub,
      cargo: payload.cargo,
      tipo: payload.tipo,
      refresh: payload.refresh,
    };
  }
}
