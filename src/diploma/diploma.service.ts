import { ConflictException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuditoriaService } from 'src/auditoria/auditoria.service';
import { CursoService } from 'src/curso/curso.service';
import { ObterPeloEMECDTO } from 'src/curso/dto/obterPeloEMEC.dto';
import { Repository } from 'typeorm';
import { Diploma } from './diploma.entity';
import { CriarDiplomaDTO } from './dto/criarDiploma.dto';
import { ObterPeloIdDTO } from './dto/obterPeloId.dto';
import { ValidarDiplomaDTO } from './dto/validarDiploma.dto';
import { Status } from './enum/status.enum';

@Injectable()
export class DiplomaService {
  constructor(
    @InjectRepository(Diploma)
    private diplomaRepository: Repository<Diploma>,
    private cursoService: CursoService,
    private auditoriaService: AuditoriaService,
  ) {}
  obterTodos(page: number | undefined) {
    if (!page) {
      return this.diplomaRepository.find();
    }
    return this.diplomaRepository.find({
      skip: page * 10,
      take: 10,
    });
  }
  obterPendentes(page: number | undefined) {
    if (!page) {
      return this.diplomaRepository.find({
        where: { status: Status.Pendente },
      });
    }
    return this.diplomaRepository.find({
      where: { status: Status.Pendente },
      order: { data: 'ASC' },
      skip: page * 10,
      take: 10,
    });
  }
  obterPeloId(obterPeloIdDTO: ObterPeloIdDTO) {
    return this.diplomaRepository.findOne(obterPeloIdDTO.id);
  }
  obterPeloEMEC(obterPeloEMECDTO: ObterPeloEMECDTO) {
    return this.diplomaRepository.find({
      where: { curso: { eMEC: obterPeloEMECDTO.eMEC } },
      order: { data: 'DESC' },
    });
  }
  async criar(criarDiplomaDTO: CriarDiplomaDTO, cpf: string) {
    const curso = await this.cursoService.obter({ eMEC: criarDiplomaDTO.eMEC });
    if (!curso) {
      throw new ConflictException('eMEC não cadastrado');
    }
    const diploma = await this.diplomaRepository.save({
      curso,
      anexo: criarDiplomaDTO.anexo,
    });
    await this.auditoriaService.criar({
      usuario: cpf,
      chave: curso.eMEC,
      tipo: 'Criar',
      novo: diploma,
    });
    return diploma;
  }
  async validar(validarDiplomaDTO: ValidarDiplomaDTO, cpf: string) {
    const diploma = await this.diplomaRepository.findOne(validarDiplomaDTO.id);
    if (!diploma) {
      throw new ConflictException('Id não cadastrado');
    }
    const novoDiploma = await this.diplomaRepository.save({
      id: diploma.id,
      ...validarDiplomaDTO,
    });
    await this.auditoriaService.criar({
      usuario: cpf,
      chave: diploma.curso.eMEC,
      tipo: 'Validar',
      antigo: diploma,
      novo: novoDiploma,
    });
    return novoDiploma;
  }
}
