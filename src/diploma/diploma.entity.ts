import { Curso } from 'src/curso/curso.entity';
import {
  BeforeInsert,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Status } from './enum/status.enum';

@Entity()
export class Diploma {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  data: Date;

  @Column({ type: 'simple-json' })
  curso: Curso;

  @Column({ default: '' })
  anexo: string;

  @Column({ default: Status.Pendente })
  status: Status;

  @Column({ nullable: true, default: null })
  motivo: string;
}
