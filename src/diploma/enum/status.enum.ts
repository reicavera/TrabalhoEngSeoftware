export enum Status {
  Pendente = 'Pendente',
  Valido = 'Válido',
  Invalido = 'Inválido',
}
