import { Module } from '@nestjs/common';
import { DiplomaService } from './diploma.service';
import { DiplomaController } from './diploma.controller';
import { Diploma } from './diploma.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CursoModule } from 'src/curso/curso.module';
import { AuditoriaModule } from 'src/auditoria/auditoria.module';

@Module({
  imports: [TypeOrmModule.forFeature([Diploma]), CursoModule, AuditoriaModule],
  providers: [DiplomaService],
  controllers: [DiplomaController],
})
export class DiplomaModule {}
