import {
  Controller,
  Get,
  Param,
  Headers,
  UseGuards,
  BadRequestException,
  Post,
  Body,
  Patch,
} from '@nestjs/common';
import { Cargos } from 'src/acesso/cargos/cargos.decorator';
import { CargoGuard } from 'src/acesso/cargos/cargos.guard';
import { UsuarioCpf } from 'src/acesso/decorator/usuario.decorator';
import { JwtAuthGuard } from 'src/acesso/jwt/jwt-auth.guard';
import { Cargo } from 'src/usuario/enum/cargos.enum';
import { DiplomaService } from './diploma.service';
import { CriarDiplomaDTO } from './dto/criarDiploma.dto';
import { ValidarDiplomaDTO } from './dto/validarDiploma.dto';

@Controller('diploma')
export class DiplomaController {
  constructor(private diplomaService: DiplomaService) {}
  @Get('/all/:page?')
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.FuncValidadora)
  obterTodos(
    @Param('page')
    page: number,
  ) {
    return this.diplomaService.obterTodos(page);
  }
  @Get('/pendentes/:page?')
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.FuncValidadora)
  obterPendentes(
    @Param('page')
    page: number,
  ) {
    return this.diplomaService.obterPendentes(page);
  }
  @Get('/eMEC')
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.FuncParceira)
  obterPeloEMAC(@Headers('eMEC') eMEC: string) {
    if (!eMEC) {
      throw new BadRequestException('header eMEC não pode ser vazio');
    }
    eMEC = eMEC.replace(/\D/g, '');
    return this.diplomaService.obterPeloEMEC({ eMEC });
  }
  @Get('/id')
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.FuncValidadora)
  obterPeloId(@Headers('id') id: number) {
    if (!id || isNaN(id)) {
      throw new BadRequestException(
        'header id não pode ser vazioou deve ser um número',
      );
    }
    return this.diplomaService.obterPeloId({ id });
  }
  @Post()
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.FuncParceira)
  criar(@Body() criarDiplomaDTO: CriarDiplomaDTO, @UsuarioCpf() cpf: string) {
    return this.diplomaService.criar(criarDiplomaDTO, cpf);
  }
  @Patch()
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.FuncValidadora)
  validar(
    @Body() validarDiplomaDTO: ValidarDiplomaDTO,
    @UsuarioCpf() cpf: string,
  ) {
    return this.diplomaService.validar(validarDiplomaDTO, cpf);
  }
}
