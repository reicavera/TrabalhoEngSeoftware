import { Transform } from 'class-transformer';
import { IsNotEmpty, IsString } from 'class-validator';
import { apenasNumeros } from 'src/usuario/formatacao/apenasNumeros.formatacao';

export class CriarDiplomaDTO {
  @IsNotEmpty()
  @IsString()
  @Transform(({ value }) => apenasNumeros(value))
  readonly eMEC: string;

  @IsNotEmpty()
  @IsString()
  readonly anexo: string;
}
