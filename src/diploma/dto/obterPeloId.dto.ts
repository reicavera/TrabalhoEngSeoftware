import { IsNotEmpty, IsNumber } from 'class-validator';

export class ObterPeloIdDTO {
  @IsNotEmpty()
  @IsNumber()
  readonly id: number;
}
