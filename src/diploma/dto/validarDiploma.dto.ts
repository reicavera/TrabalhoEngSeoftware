import { IsEnum, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { Status } from '../enum/status.enum';

export class ValidarDiplomaDTO {
  @IsNotEmpty()
  @IsNumber()
  readonly id: number;

  @IsNotEmpty()
  @IsEnum(Status)
  readonly status: Status;

  @IsNotEmpty()
  @IsString()
  readonly motivo: string;
}
