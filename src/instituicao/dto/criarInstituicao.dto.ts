import { IsEnum, IsNotEmpty, IsString } from 'class-validator';
import { Tipo } from '../enum/tipo.enum';

export class CriarInstituicaoDTO {
  @IsNotEmpty()
  @IsString()
  readonly nome: string;

  @IsNotEmpty()
  @IsEnum(Tipo)
  readonly tipo: Tipo;

  @IsNotEmpty()
  @IsString()
  readonly endereco: string;

  @IsNotEmpty()
  @IsString()
  readonly cidade: string;

  @IsNotEmpty()
  @IsString()
  readonly estado: string;

  @IsNotEmpty()
  @IsString()
  readonly credenciamento: string;

  @IsNotEmpty()
  @IsString()
  readonly mantenedora: string;
}
