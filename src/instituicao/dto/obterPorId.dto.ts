import { IsNotEmpty, IsNumber } from 'class-validator';

export class ObterPorIdDTO {
  @IsNotEmpty()
  @IsNumber()
  readonly id: number;
}
