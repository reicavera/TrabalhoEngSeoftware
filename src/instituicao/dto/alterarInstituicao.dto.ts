import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class AlterarInstituicaoDTO {
  @IsNotEmpty()
  @IsNumber()
  id: number;

  @IsOptional()
  @IsString()
  nome: string;

  @IsOptional()
  @IsString()
  endereco: string;

  @IsOptional()
  @IsString()
  cidade: string;

  @IsOptional()
  @IsString()
  estado: string;

  @IsOptional()
  @IsString()
  credenciamento: string;

  @IsOptional()
  @IsString()
  mantenedora: string;
}
