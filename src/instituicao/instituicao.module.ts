import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InstituicaoController } from './instituicao.controller';
import { Instituicao } from './instituicao.entity';
import { InstituicaoService } from './instituicao.service';

@Module({
  imports: [TypeOrmModule.forFeature([Instituicao])],
  controllers: [InstituicaoController],
  providers: [InstituicaoService],
  exports: [InstituicaoService],
})
export class InstituicaoModule {}
