import {
  ConflictException,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { Instituicao } from './instituicao.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CriarInstituicaoDTO } from './dto/criarInstituicao.dto';
import { Tipo } from './enum/tipo.enum';
import { Cargo } from 'src/usuario/enum/cargos.enum';
import { AlterarInstituicaoDTO } from './dto/alterarInstituicao.dto';
import { ObterPorIdDTO } from './dto/obterPorId.dto';

@Injectable()
export class InstituicaoService {
  constructor(
    @InjectRepository(Instituicao)
    private instituicaoRepository: Repository<Instituicao>,
  ) {}
  obterTodos(page: number) {
    return this.instituicaoRepository.find({
      relations: ['usuarios', 'cursos'],
      skip: page * 10,
      take: 10,
    });
  }
  obter(obterPorIdDTO: ObterPorIdDTO) {
    return this.instituicaoRepository.findOne({
      where: { id: obterPorIdDTO.id },
      relations: ['usuarios', 'cursos'],
    });
  }
  obterPeloId(obterPorIdDTO: ObterPorIdDTO) {
    return this.instituicaoRepository.findOne({
      where: { id: obterPorIdDTO.id },
    });
  }
  async criar(criarInstituicaoDTO: CriarInstituicaoDTO, cargo: Cargo) {
    if (this.naoAutorizadoCriar(cargo, criarInstituicaoDTO.tipo)) {
      throw new ForbiddenException(
        cargo + ' não é autorizado a criar ' + criarInstituicaoDTO.tipo,
      );
    }
    const instituicao = await this.instituicaoRepository.save(
      criarInstituicaoDTO,
    );
    if (criarInstituicaoDTO.tipo === Tipo.Parceira) {
      instituicao.acesso = false;
    }
    return this.instituicaoRepository.save(instituicao);
  }
  async alterar(alterarInstituicaoDTO: AlterarInstituicaoDTO, cargo: Cargo) {
    const instituicao = await this.instituicaoRepository.findOne(
      alterarInstituicaoDTO.id,
    );
    if (this.naoAutorizadoAlterar(cargo, instituicao.tipo)) {
      throw new ForbiddenException(
        cargo + ' não é autorizado a criar ' + instituicao.tipo,
      );
    }
    return this.instituicaoRepository.save(alterarInstituicaoDTO);
  }
  async liberarAcesso(obterPorIdDTO: ObterPorIdDTO) {
    const instituicao = await this.instituicaoRepository.findOne(
      obterPorIdDTO.id,
    );
    if (instituicao.tipo === Tipo.Validadora) {
      throw new ConflictException(
        'Só é possível liberar o acesso de Instituições Parceiras',
      );
    }
    return this.instituicaoRepository.save({
      id: instituicao.id,
      acesso: true,
    });
  }
  naoAutorizadoCriar(cargo: Cargo, tipo: Tipo) {
    return !(
      (cargo === Cargo.Superintendente && tipo === Tipo.Validadora) ||
      (cargo === Cargo.Dirigente && tipo === Tipo.Parceira)
    );
  }
  naoAutorizadoAlterar(cargo: Cargo, tipo: Tipo) {
    return !(
      (cargo === Cargo.Superintendente && tipo === Tipo.Validadora) ||
      (cargo === Cargo.Diretor && tipo === Tipo.Parceira)
    );
  }
}
