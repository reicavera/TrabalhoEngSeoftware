import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { Cargos } from 'src/acesso/cargos/cargos.decorator';
import { CargoGuard } from 'src/acesso/cargos/cargos.guard';
import { UsuarioCargo } from 'src/acesso/decorator/usuario.decorator';
import { JwtAuthGuard } from 'src/acesso/jwt/jwt-auth.guard';
import { Cargo } from 'src/usuario/enum/cargos.enum';
import { AlterarInstituicaoDTO } from './dto/alterarInstituicao.dto';
import { CriarInstituicaoDTO } from './dto/criarInstituicao.dto';
import { InstituicaoService } from './instituicao.service';

@Controller('instituicao')
export class InstituicaoController {
  constructor(private instituicaoService: InstituicaoService) {}
  @Get('/all/:page?')
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.Superintendente)
  obterTodos(
    @Param('page')
    page: number,
  ) {
    return this.instituicaoService.obterTodos(page);
  }
  @Get()
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.Superintendente)
  obter(@Headers('id') id: number) {
    if (!id || isNaN(id)) {
      throw new BadRequestException(
        'header id não pode ser vazio ou deve ser um número',
      );
    }
    return this.instituicaoService.obter({ id });
  }
  @Patch('/liberarAcesso')
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.Superintendente)
  liberarAcesso(@Headers('id') id: number) {
    if (!id || isNaN(id)) {
      throw new BadRequestException(
        'header id não pode ser vazio ou deve ser um número',
      );
    }
    return this.instituicaoService.liberarAcesso({ id });
  }
  @Post()
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.Dirigente, Cargo.Superintendente)
  criar(
    @Body() criarInstituicaoDTO: CriarInstituicaoDTO,
    @UsuarioCargo() cargo: Cargo,
  ) {
    return this.instituicaoService.criar(criarInstituicaoDTO, cargo);
  }
  @Patch()
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.Diretor, Cargo.Superintendente)
  alterar(
    @Body() alterarInstituicaoDTO: AlterarInstituicaoDTO,
    @UsuarioCargo() cargo: Cargo,
  ) {
    return this.instituicaoService.alterar(alterarInstituicaoDTO, cargo);
  }
}
