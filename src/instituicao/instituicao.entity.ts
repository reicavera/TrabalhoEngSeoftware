import { Curso } from 'src/curso/curso.entity';
import { Usuario } from 'src/usuario/usuario.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Tipo } from './enum/tipo.enum';

@Entity()
export class Instituicao {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nome: string;

  @Column()
  tipo: Tipo;

  @Column()
  endereco: string;

  @Column()
  cidade: string;

  @Column()
  estado: string;

  @Column()
  credenciamento: string;

  @Column()
  mantenedora: string;

  @Column({ nullable: true, default: null })
  acesso: boolean;

  @OneToMany(() => Usuario, (usuario) => usuario.instituicao)
  usuarios: Usuario[];

  @OneToMany(() => Curso, (curso) => curso.instituicao)
  cursos: Curso[];
}
