import { Exclude } from 'class-transformer';
import { Instituicao } from 'src/instituicao/instituicao.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Cargo } from './enum/cargos.enum';

@Entity()
export class Usuario {
  @PrimaryGeneratedColumn()
  @Exclude()
  id: number;

  @Column({ nullable: false, unique: true })
  cpf: string;

  @Column()
  cargo: Cargo;

  @Column()
  nome: string;

  @Column()
  sobrenome: string;

  @Column()
  telefone: string;

  @Column()
  email: string;

  @Column({ nullable: false })
  @Exclude()
  senha: string;

  @Column({ nullable: true })
  @Exclude()
  refresh: string;

  @ManyToOne(() => Instituicao, (instituicao) => instituicao.id)
  instituicao: Instituicao;
}
