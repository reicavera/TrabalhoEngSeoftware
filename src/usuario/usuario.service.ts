import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Cargo } from './enum/cargos.enum';
import { CriarUsuarioDTO } from './dto/criarUsuario.dto';
import { Usuario } from './usuario.entity';
import * as bcrypt from 'bcrypt';
import { ObterPeloCpfDTO } from './dto/obterPeloCpf.dto';
import { gerarNovaSenha } from '../email/email.service';
import { AlterarUsuarioDTO } from './dto/alterarUsuario.dto';
import { RedefinirSenhaDTO } from 'src/acesso/dto/redefinirSenha.dto';
import { InstituicaoService } from 'src/instituicao/instituicao.service';
import { ObterPorIdDTO } from 'src/instituicao/dto/obterPorId.dto';

@Injectable()
export class UsuarioService {
  constructor(
    @InjectRepository(Usuario)
    private usuarioRepository: Repository<Usuario>,
    private instituicaoService: InstituicaoService,
  ) {}
  async criar(criarUsuarioDTO: CriarUsuarioDTO, cargo: Cargo) {
    if (this.naoAutorizado(cargo, criarUsuarioDTO.cargo)) {
      throw new ForbiddenException(
        cargo + ' não é autorizado a criar ' + criarUsuarioDTO.cargo,
      );
    }
    let usuario = await this.usuarioRepository.findOne({
      where: { cpf: criarUsuarioDTO.cpf },
    });
    if (usuario) {
      throw new ConflictException('CPF já cadastrado');
    }
    const numerosAleatorios = gerarNovaSenha(criarUsuarioDTO.email);
    const senha = bcrypt.hashSync(numerosAleatorios, 8);
    usuario = await this.usuarioRepository.create(criarUsuarioDTO);
    let instituicao;
    if (![undefined, null].includes(criarUsuarioDTO.instituicaoId)) {
      instituicao = await this.instituicaoService.obter({
        id: criarUsuarioDTO.instituicaoId,
      });
    }
    if (criarUsuarioDTO.instituicaoId === null) {
      instituicao = null;
    }
    usuario = { ...usuario, senha, instituicao };
    await this.usuarioRepository.save(usuario);
    return this.usuarioRepository.findOne(usuario.id, {
      relations: ['instituicao'],
    });
  }
  async alterar(alterarUsuarioDTO: AlterarUsuarioDTO, cargo: Cargo) {
    const usuario = await this.usuarioRepository.findOne({
      where: { cpf: alterarUsuarioDTO.cpf },
    });
    if (!usuario) {
      throw new ConflictException('CPF não identificado');
    }
    if (this.naoAutorizado(cargo, usuario.cargo)) {
      throw new ForbiddenException(
        cargo + ' não é autorizado a alterar ' + usuario.cargo,
      );
    }
    let instituicao;
    if (![undefined, null].includes(alterarUsuarioDTO.instituicaoId)) {
      instituicao = await this.instituicaoService.obter({
        id: alterarUsuarioDTO.instituicaoId,
      });
    }
    if (alterarUsuarioDTO.instituicaoId === null) {
      instituicao = null;
    }
    await this.usuarioRepository.save({
      ...alterarUsuarioDTO,
      id: usuario.id,
      instituicao,
    });
    return this.usuarioRepository.findOne(usuario.id, {
      relations: ['instituicao'],
    });
  }
  alterarRefresh(id: number, refresh: string) {
    this.usuarioRepository.save({ id, refresh });
  }

  async remover(obterPeloCPF: ObterPeloCpfDTO, cargo: Cargo) {
    let usuario = undefined;
    if (cargo === Cargo.Diretor) {
      usuario = await this.usuarioRepository.findOne({
        where: [
          { cargo: Cargo.Diretor, cpf: obterPeloCPF.cpf },
          { cargo: Cargo.Dirigente, cpf: obterPeloCPF.cpf },
          { cargo: Cargo.FuncParceira, cpf: obterPeloCPF.cpf },
        ],
      });
    }
    if (cargo === Cargo.Superintendente) {
      usuario = await this.usuarioRepository.findOne({
        where: [
          { cargo: Cargo.Superintendente, cpf: obterPeloCPF.cpf },
          { cargo: Cargo.Dirigente, cpf: obterPeloCPF.cpf },
          { cargo: Cargo.Coordenador, cpf: obterPeloCPF.cpf },
          { cargo: Cargo.FuncValidadora, cpf: obterPeloCPF.cpf },
        ],
      });
    }
    if (!usuario) {
      throw new ConflictException(
        'CPF não encontrado ou não corresponde ao cargo de ' + cargo,
      );
    }
    if (this.naoAutorizado(cargo, usuario.cargo)) {
      throw new ForbiddenException(
        cargo + ' não é autorizado a alterar ' + usuario.cargo,
      );
    }
    this.usuarioRepository.delete(usuario.id);
  }
  obterTodos(page: number | undefined, cargo: Cargo) {
    if (cargo === Cargo.Diretor) {
      return this.usuarioRepository.find({
        where: [
          { cargo: Cargo.Diretor },
          { cargo: Cargo.Dirigente },
          { cargo: Cargo.FuncParceira },
        ],
        relations: ['instituicao'],
        order: { nome: 'ASC', sobrenome: 'ASC' },
        skip: page * 10,
        take: 10,
      });
    }
    if (!page) {
      return this.usuarioRepository.find({
        where: [
          { cargo: Cargo.Superintendente },
          { cargo: Cargo.Coordenador },
          { cargo: Cargo.FuncValidadora },
          { cargo: Cargo.Dirigente },
        ],
        relations: ['instituicao'],
        order: { nome: 'ASC', sobrenome: 'ASC' },
      });
    }
    return this.usuarioRepository.find({
      where: [
        { cargo: Cargo.Superintendente },
        { cargo: Cargo.Coordenador },
        { cargo: Cargo.FuncValidadora },
        { cargo: Cargo.Dirigente },
      ],
      relations: ['instituicao'],
      order: { nome: 'ASC', sobrenome: 'ASC' },
      skip: page * 10,
      take: 10,
    });
  }
  obterPeloCPF(obterPeloCPF: ObterPeloCpfDTO) {
    return this.usuarioRepository.findOne({
      where: { cpf: obterPeloCPF.cpf },
    });
  }
  obterPelaInstituicao(obterPorIdDTO: ObterPorIdDTO) {
    return this.usuarioRepository.find({
      where: { instituicao: { id: obterPorIdDTO.id } },
    });
  }
  obter(obterPeloCPF: ObterPeloCpfDTO) {
    return this.usuarioRepository.findOne({
      where: { cpf: obterPeloCPF.cpf },
      relations: ['instituicao'],
    });
  }
  async novaSenha(cpf: string) {
    const usuario = await this.usuarioRepository.findOne({ where: { cpf } });
    if (!usuario) {
      throw new NotFoundException('CPF não encontrado');
    }
    const senha = bcrypt.hashSync(gerarNovaSenha(usuario.email), 8);
    this.usuarioRepository.save({ id: usuario.id, senha });
  }
  async redefinirSenha(cpf: string, redefinirSenhaDTO: RedefinirSenhaDTO) {
    const usuario = await this.usuarioRepository.findOne({ where: { cpf } });
    if (!usuario) {
      throw new NotFoundException('CPF não encontrado');
    }
    const senha = bcrypt.hashSync(gerarNovaSenha(redefinirSenhaDTO.senha), 8);
    this.usuarioRepository.save({ id: usuario.id, senha });
  }
  naoAutorizado(cargoUsuario: Cargo, cargoMudanca: Cargo) {
    return (
      (cargoUsuario === Cargo.Superintendente &&
        [Cargo.Diretor, Cargo.FuncParceira].includes(cargoMudanca)) ||
      (cargoUsuario === Cargo.Diretor &&
        [
          Cargo.Superintendente,
          Cargo.Coordenador,
          Cargo.FuncValidadora,
        ].includes(cargoMudanca))
    );
  }
}
