import { Transform } from 'class-transformer';
import { IsNotEmpty, IsString } from 'class-validator';
import { apenasNumeros } from '../formatacao/apenasNumeros.formatacao';

export class ObterPeloCpfDTO {
  @IsNotEmpty()
  @IsString()
  @Transform(({ value }) => apenasNumeros(value))
  readonly cpf: string;
}
