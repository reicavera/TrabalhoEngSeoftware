import { Transform } from 'class-transformer';
import {
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';
import { Cargo } from '../enum/cargos.enum';
import { apenasNumeros } from '../formatacao/apenasNumeros.formatacao';

export class CriarUsuarioDTO {
  @IsNotEmpty()
  @IsString()
  readonly nome: string;

  @IsNotEmpty()
  @IsEnum(Cargo)
  readonly cargo: Cargo;

  @IsNotEmpty()
  @IsString()
  readonly sobrenome: string;

  @IsNotEmpty()
  @IsString()
  @Transform(({ value }) => apenasNumeros(value))
  readonly cpf: string;

  @IsNotEmpty()
  @IsString()
  @Transform(({ value }) => apenasNumeros(value))
  readonly telefone: string;

  @IsNotEmpty()
  @IsString()
  @IsEmail()
  readonly email: string;

  @IsOptional()
  readonly instituicaoId: number | null;
}
