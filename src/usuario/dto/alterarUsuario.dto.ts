import { Transform } from 'class-transformer';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { apenasNumeros } from '../formatacao/apenasNumeros.formatacao';

export class AlterarUsuarioDTO {
  @IsNotEmpty()
  @IsString()
  @Transform(({ value }) => apenasNumeros(value))
  readonly cpf: string;

  @IsOptional()
  @IsString()
  readonly nome?: string;

  @IsOptional()
  @IsString()
  readonly sobrenome?: string;

  @IsOptional()
  @IsString()
  @Transform(({ value }) => apenasNumeros(value))
  readonly telefone?: string;

  @IsOptional()
  readonly instituicaoId?: number | null;
}
