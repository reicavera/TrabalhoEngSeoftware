export const apenasNumeros = (valor: string) => {
  return valor.replace(/\D/g, '');
};
