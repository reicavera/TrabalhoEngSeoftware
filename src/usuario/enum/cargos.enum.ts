export enum Cargo {
  Dirigente = 'Dirigente',
  Diretor = 'Diretor',
  Superintendente = 'Superintendente',
  Coordenador = 'Coordenador',
  FuncValidadora = 'Funcionário de Instituição Validadora',
  FuncParceira = 'Funcionário de Instituição Parceira',
}
