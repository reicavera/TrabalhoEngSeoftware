import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Patch,
  UseGuards,
  Delete,
  Headers,
  BadRequestException,
} from '@nestjs/common';
import { CriarUsuarioDTO } from './dto/criarUsuario.dto';
import { UsuarioService } from './usuario.service';
import { JwtAuthGuard } from 'src/acesso/jwt/jwt-auth.guard';
import { Cargos } from '../acesso/cargos/cargos.decorator';
import { Cargo } from './enum/cargos.enum';
import { CargoGuard } from 'src/acesso/cargos/cargos.guard';
import {
  UsuarioCargo,
  UsuarioCpf,
} from 'src/acesso/decorator/usuario.decorator';
import { AlterarUsuarioDTO } from './dto/alterarUsuario.dto';
@Controller('usuario')
export class UsuarioController {
  constructor(private usuarioService: UsuarioService) {}
  @Get('/all/:page?')
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.Diretor, Cargo.Superintendente)
  obterTodos(
    @Param('page')
    page: number,
    @UsuarioCargo() cargo: Cargo,
  ) {
    return this.usuarioService.obterTodos(page, cargo);
  }
  @Get('/instituicaoId')
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.Superintendente, Cargo.Diretor)
  obterPelaInstituicao(@Headers('id') id: number) {
    if (!id || isNaN(id)) {
      throw new BadRequestException(
        'header id não pode ser vazio ou deve ser um número',
      );
    }
    return this.usuarioService.obterPelaInstituicao({ id });
  }
  @Get('/obter')
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.Diretor, Cargo.Superintendente)
  obter(@Headers('cpf') cpf: string) {
    if (!cpf) {
      throw new BadRequestException('header cpf não pode ser vazio');
    }
    cpf = cpf.replace(/\D/g, '');
    return this.usuarioService.obter({ cpf });
  }
  @Get()
  @UseGuards(JwtAuthGuard)
  @Cargos(Cargo.Diretor, Cargo.Superintendente)
  obterProprio(@UsuarioCpf() cpf: string) {
    if (!cpf) {
      throw new BadRequestException('header cpf não pode ser vazio');
    }
    cpf = cpf.replace(/\D/g, '');
    return this.usuarioService.obter({ cpf });
  }
  @Post()
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.Diretor, Cargo.Superintendente)
  criar(
    @Body() criarUsuarioDTO: CriarUsuarioDTO,
    @UsuarioCargo() cargo: Cargo,
  ) {
    return this.usuarioService.criar(criarUsuarioDTO, cargo);
  }

  @Patch()
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.Diretor, Cargo.Superintendente)
  alterar(
    @Body() alterarUsuarioDTO: AlterarUsuarioDTO,
    @UsuarioCargo() cargo: Cargo,
  ) {
    return this.usuarioService.alterar(alterarUsuarioDTO, cargo);
  }

  @Delete()
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.Diretor, Cargo.Superintendente)
  remover(@Headers('cpf') cpf: string, @UsuarioCargo() cargo: Cargo) {
    if (!cpf) {
      throw new BadRequestException('header cpf não pode ser vazio');
    }
    cpf = cpf.replace(/\D/g, '');
    return this.usuarioService.remover({ cpf }, cargo);
  }
}
