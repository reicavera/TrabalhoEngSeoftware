import { CacheModule, Module } from '@nestjs/common';
import { AcessoModule } from './acesso/acesso.module';
import { UsuarioModule } from './usuario/usuario.module';
import { InstituicaoModule } from './instituicao/instituicao.module';
import { CursoModule } from './curso/curso.module';
import { DiplomaModule } from './diploma/diploma.module';
import { AuditoriaModule } from './auditoria/auditoria.module';
import { Usuario } from './usuario/usuario.entity';
import { Instituicao } from './instituicao/instituicao.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import 'dotenv/config';
import { Curso } from './curso/curso.entity';
import { Diploma } from './diploma/diploma.entity';
import { Auditoria } from './auditoria/auditoria.entity';
@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DB_HOST,
      port: parseInt(process.env.DB_PORT) || 5433,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
      entities: [Usuario, Instituicao, Curso, Diploma, Auditoria],
      synchronize: true,
    }),
    AcessoModule,
    UsuarioModule,
    InstituicaoModule,
    CursoModule,
    DiplomaModule,
    AuditoriaModule,
  ],
})
export class AppModule {}
