import * as SendGrid from '@sendgrid/mail';
import { randomInt } from 'crypto';
import 'dotenv/config';
SendGrid.setApiKey(process.env.SG_API_KEY);
function gerarNovaSenha(email: string) {
  const senha = randomInt(1000_000).toString().padStart(6, '0');
  const text =
    'Sua nova senha para usar na aplicação é ' +
    senha +
    '. Uma mudança de senha é recomendada.';
  const message = {
    to: email,
    from: process.env.SG_SENDER,
    subject: 'Nova Senha',
    text,
    html: '<h1>' + text + '</h1>',
  };

  SendGrid.send(message);
  return senha;
}
function autenticarEmail(email: string, token: string) {
  const text = 'Token para confirmar nova senha: ' + token;
  const message = {
    to: email,
    from: process.env.SG_SENDER,
    subject: 'Confirmar Nova senha',
    text,
    html: '<h1>' + text + '</h1>',
  };
  SendGrid.send(message);
}
export { gerarNovaSenha, autenticarEmail };
