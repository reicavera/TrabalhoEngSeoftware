import {
  Controller,
  Get,
  Param,
  Headers,
  UseGuards,
  BadRequestException,
} from '@nestjs/common';
import { Cargos } from 'src/acesso/cargos/cargos.decorator';
import { CargoGuard } from 'src/acesso/cargos/cargos.guard';
import { JwtAuthGuard } from 'src/acesso/jwt/jwt-auth.guard';
import { Cargo } from 'src/usuario/enum/cargos.enum';
import { AuditoriaService } from './auditoria.service';

@Controller('auditoria')
export class AuditoriaController {
  constructor(private auditoriaService: AuditoriaService) {}
  @Get('/all/:page?')
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.Coordenador)
  obterTodos(
    @Param('page')
    page: number,
  ) {
    return this.auditoriaService.obterTodos(page);
  }
  @Get('/obterPorUsuario')
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.Coordenador)
  obterPeloUsuario(@Headers('cpf') cpf: string) {
    if (!cpf) {
      throw new BadRequestException('header cpf não pode ser vazio');
    }
    cpf = cpf.replace(/\D/g, '');
    return this.auditoriaService.obterPorUsuario({ cpf });
  }
  @Get('/obterPorCurso')
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.Coordenador)
  obterPorCurso(@Headers('eMEC') eMEC: string) {
    if (!eMEC) {
      throw new BadRequestException('header eMEC não pode ser vazio');
    }
    eMEC = eMEC.replace(/\D/g, '');
    return this.auditoriaService.obterPorCurso({ eMEC });
  }
}
