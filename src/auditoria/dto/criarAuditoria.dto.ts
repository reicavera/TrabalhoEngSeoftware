import { Diploma } from 'src/diploma/diploma.entity';

export class CriarAuditoriaDTO {
  readonly usuario: string;

  readonly chave: string;

  readonly tipo: string;

  readonly antigo?: Diploma;

  readonly novo?: Diploma;
}
