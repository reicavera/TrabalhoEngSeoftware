import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ObterPeloEMECDTO } from 'src/curso/dto/obterPeloEMEC.dto';
import { ObterPeloCpfDTO } from 'src/usuario/dto/obterPeloCpf.dto';
import { Repository } from 'typeorm';
import { Auditoria } from './auditoria.entity';
import { CriarAuditoriaDTO } from './dto/criarAuditoria.dto';

@Injectable()
export class AuditoriaService {
  constructor(
    @InjectRepository(Auditoria)
    private auditoriaRepository: Repository<Auditoria>,
  ) {}
  obterTodos(page: number | undefined) {
    if (!page) {
      return this.auditoriaRepository.find({ order: { data: 'DESC' } });
    }
    return this.auditoriaRepository.find({
      order: { data: 'DESC' },
      skip: page * 10,
      take: 10,
    });
  }
  obterPorUsuario(obterPeloCpfDTO: ObterPeloCpfDTO) {
    return this.auditoriaRepository.find({
      where: { usuario: obterPeloCpfDTO.cpf },
    });
  }
  obterPorCurso(obterPeloEMECDTO: ObterPeloEMECDTO) {
    return this.auditoriaRepository.find({
      where: { chave: obterPeloEMECDTO.eMEC },
    });
  }
  criar(criarAuditoriaDTO: CriarAuditoriaDTO) {
    return this.auditoriaRepository.save(criarAuditoriaDTO);
  }
}
