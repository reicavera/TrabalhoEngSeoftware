import { Curso } from 'src/curso/curso.entity';
import { Diploma } from 'src/diploma/diploma.entity';
import {
  BeforeInsert,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Auditoria {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  data: Date;

  @Column()
  usuario: string;

  @Column()
  tipo: string;

  @Column()
  chave: string;

  @Column({ type: 'simple-json', nullable: true })
  antigo: Diploma;

  @Column({ type: 'simple-json', nullable: true })
  novo: Diploma;

  @BeforeInsert()
  inserirData() {
    this.data = new Date();
  }
}
