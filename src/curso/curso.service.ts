import { ConflictException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ObterPorIdDTO } from 'src/instituicao/dto/obterPorId.dto';
import { InstituicaoService } from 'src/instituicao/instituicao.service';
import { Repository } from 'typeorm';
import { Curso } from './curso.entity';
import { AlterarCursoDTO } from './dto/alterarCurso.dto';
import { CriarCursoDTO } from './dto/criarCurso.dto';
import { ObterPeloEMECDTO } from './dto/obterPeloEMEC.dto';

@Injectable()
export class CursoService {
  constructor(
    @InjectRepository(Curso)
    private cursoRepository: Repository<Curso>,
    private instituicaoService: InstituicaoService,
  ) {}
  obterTodos(page: number | undefined) {
    if (!page) {
      return this.cursoRepository.find({ order: { eMEC: 'ASC' } });
    }
    return this.cursoRepository.find({
      order: { eMEC: 'ASC' },
      skip: page * 10,
      take: 10,
    });
  }
  obter(obterPeloEMECDTO: ObterPeloEMECDTO) {
    return this.cursoRepository.findOne({
      where: { eMEC: obterPeloEMECDTO.eMEC },
    });
  }
  obterPelaInstituicao(obterPorId: ObterPorIdDTO) {
    return this.cursoRepository.find({
      where: { instituicao: { id: obterPorId.id } },
    });
  }
  async criar(criarCursoDTO: CriarCursoDTO) {
    let curso = await this.cursoRepository.findOne({
      where: { eMEC: criarCursoDTO.eMEC },
    });
    if (curso) {
      throw new ConflictException('eMEC ja cadastrado');
    }
    const instituicao = await this.instituicaoService.obterPeloId({
      id: criarCursoDTO.instituicaoId,
    });
    if (!instituicao) {
      throw new ConflictException('Instituição não encontrada');
    }
    curso = await this.cursoRepository.save({
      ...criarCursoDTO,
      instituicao,
    });
    return this.cursoRepository.findOne(curso.id);
  }
  async alterar(alterarCursoDTO: AlterarCursoDTO) {
    let curso = await this.cursoRepository.findOne({
      where: { eMEC: alterarCursoDTO.eMEC },
    });
    if (!curso) {
      throw new ConflictException('eMEC não cadastrado');
    }
    curso = await this.cursoRepository.save({
      id: curso.id,
      ...alterarCursoDTO,
    });
    return this.cursoRepository.findOne(curso.id);
  }
  async remover(obterPeloEMEC: ObterPeloEMECDTO) {
    const curso = await this.cursoRepository.findOne({
      where: { eMEC: obterPeloEMEC.eMEC },
    });
    if (!curso) {
      throw new ConflictException('eMEC não cadastrado');
    }
    this.cursoRepository.delete(curso.id);
  }
}
