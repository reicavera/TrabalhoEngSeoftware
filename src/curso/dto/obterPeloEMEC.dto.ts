import { IsNotEmpty, IsString } from 'class-validator';

export class ObterPeloEMECDTO {
  @IsNotEmpty()
  @IsString()
  readonly eMEC: string;
}
