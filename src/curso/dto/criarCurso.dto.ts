import { Transform } from 'class-transformer';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { apenasNumeros } from 'src/usuario/formatacao/apenasNumeros.formatacao';

export class CriarCursoDTO {
  @IsNotEmpty()
  @IsString()
  @Transform(({ value }) => apenasNumeros(value))
  eMEC: string;

  @IsNotEmpty()
  @IsString()
  nome: string;

  @IsNotEmpty()
  @IsString()
  readonly grau: string;

  @IsNotEmpty()
  @IsNumber()
  readonly instituicaoId: number;

  @IsNotEmpty()
  @IsString()
  readonly autorização: string;

  @IsNotEmpty()
  @IsString()
  readonly reconhecimento: string;

  @IsNotEmpty()
  @IsString()
  readonly renovação: string;

  @IsNotEmpty()
  @IsString()
  readonly observação: string;
}
