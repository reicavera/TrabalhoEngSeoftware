import { Transform } from 'class-transformer';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { apenasNumeros } from 'src/usuario/formatacao/apenasNumeros.formatacao';

export class AlterarCursoDTO {
  @IsOptional()
  @IsString()
  @Transform(({ value }) => apenasNumeros(value))
  readonly eMEC: string;

  @IsOptional()
  @IsString()
  readonly nome: string;

  @IsOptional()
  @IsString()
  readonly grau: string;

  @IsNotEmpty()
  @IsString()
  readonly autorização: string;

  @IsOptional()
  @IsString()
  readonly reconhecimento: string;

  @IsOptional()
  @IsString()
  readonly renovação: string;

  @IsOptional()
  @IsString()
  readonly observação: string;
}
