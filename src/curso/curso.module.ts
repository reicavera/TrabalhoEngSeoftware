import { Module } from '@nestjs/common';
import { CursoService } from './curso.service';
import { CursoController } from './curso.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Curso } from './curso.entity';
import { InstituicaoModule } from 'src/instituicao/instituicao.module';

@Module({
  imports: [TypeOrmModule.forFeature([Curso]), InstituicaoModule],
  providers: [CursoService],
  controllers: [CursoController],
  exports: [CursoService],
})
export class CursoModule {}
