import {
  BadRequestException,
  Controller,
  Get,
  Param,
  UseGuards,
  Headers,
  Post,
  Body,
  Patch,
  Delete,
} from '@nestjs/common';
import { Cargos } from 'src/acesso/cargos/cargos.decorator';
import { CargoGuard } from 'src/acesso/cargos/cargos.guard';
import { JwtAuthGuard } from 'src/acesso/jwt/jwt-auth.guard';
import { Cargo } from 'src/usuario/enum/cargos.enum';
import { CursoService } from './curso.service';
import { AlterarCursoDTO } from './dto/alterarCurso.dto';
import { CriarCursoDTO } from './dto/criarCurso.dto';

@Controller('curso')
export class CursoController {
  constructor(private cursoService: CursoService) {}
  @Get('/all/:page?')
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.FuncParceira)
  obterTodos(
    @Param('page')
    page: number,
  ) {
    return this.cursoService.obterTodos(page);
  }
  @Get('/instituicaoId')
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.FuncParceira)
  obterPelaInstituicao(@Headers('id') id: number) {
    if (!id || isNaN(id)) {
      throw new BadRequestException(
        'header id não pode ser vazio ou deve ser um número',
      );
    }
    return this.cursoService.obterPelaInstituicao({ id });
  }
  @Get()
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.FuncParceira)
  obter(@Headers('eMEC') eMEC: string) {
    if (!eMEC) {
      throw new BadRequestException('header eMEC não pode ser vazio');
    }
    eMEC = eMEC.replace(/\D/g, '');
    return this.cursoService.obter({ eMEC });
  }
  @Post()
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.FuncParceira)
  criar(@Body() criarCursoDTO: CriarCursoDTO) {
    this.cursoService.criar(criarCursoDTO);
  }
  @Patch()
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.FuncParceira)
  alterar(@Body() alterarCursoDTO: AlterarCursoDTO) {
    this.cursoService.alterar(alterarCursoDTO);
  }
  @Delete()
  @UseGuards(JwtAuthGuard, CargoGuard)
  @Cargos(Cargo.FuncParceira)
  remover(@Headers('eMEC') eMEC: string) {
    if (!eMEC) {
      throw new BadRequestException('header eMEC não pode ser vazio');
    }
    eMEC = eMEC.replace(/\D/g, '');
    return this.cursoService.remover({ eMEC });
  }
}
