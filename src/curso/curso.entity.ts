import { Exclude } from 'class-transformer';
import { Instituicao } from 'src/instituicao/instituicao.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Curso {
  @PrimaryGeneratedColumn()
  @Exclude()
  id: number;

  @Column({ unique: true })
  eMEC: string;

  @Column()
  nome: string;

  @Column()
  grau: string;

  @Column()
  autorização: string;

  @Column()
  reconhecimento: string;

  @Column()
  renovação: string;

  @Column()
  observação: string;

  @ManyToOne(() => Instituicao, (instituicao) => instituicao.id)
  instituicao: Instituicao;
}
