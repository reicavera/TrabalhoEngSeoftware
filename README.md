## Description

Back-End construido por Matheus Alves Guimarães para o trabalho de Engenharia de Software 1 usando NestJS, um framework do Node.js.
Para utilização, é necessário ter um banco de dados PostgresQL (É possível utilizar o MySQL alterando 
as informações type do TypeORM no src/app.module.ts, não foi testado para outros bancos de dados) e
uma conta no SendGrid com um email mensageiro (aprovado pelo SendGrid) para o envio de emails, essencial
para a criação de um Usuário.

Como toda criação de Usuário é feita a partir de outro Usuário, é necessário adicionar um primeiro no
banco de dados. Importante salientar que a senha é guardada em Hash e por isso deve-se criptografar
manualmente na criação do primeiro Usuário. Tal ação pode ser feita em https://bcrypt-generator.com ,
usando 8 Rounds.

## Tecnologias Utilizadas no Projeto

- Typescript
- Node.js
- Nestjs
- SendGrid
- TypeORM
- PostgresQL (MySQL funciona com alterações)
- JWT
## Links Importantes
- [Documentação sobre as rotas da aplicação](https://documenter.getpostman.com/view/16175331/UzBguUbi)
- [Mais informações sobre o trabalho](https://docs.google.com/document/d/1JHKkvD33G7qIt5yucPLxs6Yev-PWmV6JlLcCdtfj6Cs/edit?usp=sharing)
- [Front-End produzido por Natan Moura](https://github.com/ntsmoura/diploma-frontend)
- [Histórias de Usuário do projeto](https://docs.google.com/document/d/1nukuGDdoNzkUHeab0Y0Am0CFwu8AzCv8Z_pFk6i1bKQ/edit?usp=sharing)

## Instalação

É necessário o possuir o [Node.js](https://nodejs.org/en/) para instalação do Projeto:

```bash
$ npm install
```
## Variáveis de ambiente

É necessário criar um arquivo .env com as seguintes variáveis:

```bash
DB_HOST="host do banco de dados"
DB_PORT="port do banco de dados"
DB_USERNAME="usuário do banco de dados"
DB_PASSWORD="senha do banco de dados"
DB_DATABASE="schema da banco de dados"
JWT_SECRET="De preferência,coloque uma string aleatoria"
SG_API_KEY="API KEY do SendGrid"
SG_SENDER="email que enviará através do SendGrid."
```

## Rodar a aplicação

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod

# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
